Teamspeak extension for phpbb 3.1.x
========================

[https://bitbucket.org/matthieuy/phpbb-ts3](https://bitbucket.org/matthieuy/phpbb-ts3)

Install
============

Download this code and copy it in your phpbb install under the `ext` directory.
You should have this path : `phpbb_install/ext/matthieuy/teamspeak`

- Go into your ACP => Customise => Manage extension
- Enable this extension
- Enjoy

Develop
=======

You can propose pull request or open a issue if you need more informations.

Licence :
=========

This code is under [GNU GPL 2.0](http://opensource.org/licenses/gpl-2.0.php)

Changelog
=========

- v1.0.0 (08/30/2015) :
    - First commit

TODO
====

- Generate key privilege
- Unit tests
